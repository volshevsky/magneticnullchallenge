# Magnetic Null Challenge #

Methods for the Magnetic Null Challenge proposed within our 2017-2018 ISSI team [Magnetic Topology Effects on Energy Dissipation in Turbulent Plasma](https://www.issibern.ch/teams/energydissip/).

### Getting Started ###

TBD

### Citing ###

TBD

### Data formats ###

#### Trajectory data ####

The data is stored in single precision, in 1000x4 arrays: x, y, z, bx, by, bz, jx, jy, jz.

### Who do I talk to? ###

Vyacheslav Olshevsky (sya@mao.kiev.ua)