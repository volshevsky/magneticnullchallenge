"""
    VTK routines. Based on write* from proc2vtr.py.
    Those are for postprocessing of iPIC3D data.

    (c) 04.2013, V. Olshevsky, email: sya@mao.kiev.ua

"""
import numpy as np
import scipy.io
import xml.dom.minidom
import vtk
import os
## A workout for python distributions that don't have vtk_to_numpy analog:
#from tvtk import array_handler  # convert vtk2array
from vtk.util import numpy_support  # it's from tvtk actually
from scipy.io import FortranFile


def fname_suffix(i, fill_zeros=8):
    """ Returns a string representation of the number for the file name. """
    if fill_zeros > 0:
        s = str(fill_zeros)
        return ('%' + s + '.' + s + 'i') % i
    else:
        return repr(i)


def writePVD(pvd_name, list_pvtr, list_cycle):
    """write the pvd (VTK header file) file that contains all .pvtr file names

       Parameters:
        pvd_name -- outpur file name
        list_pvtr -- list of pvtr file names
        list_cycle -- cycles that correspond to pvtr files.
                      A list of (timesteps of) the same length as list_pvtr

    """
    pvd = xml.dom.minidom.Document()
    pvd_root = pvd.createElementNS("VTK", "VTKFile")
    pvd_root.setAttribute("type", "Collection")
    pvd_root.setAttribute("version", "0.1")
    pvd_root.setAttribute("byte_order", "LittleEndian")
    pvd.appendChild(pvd_root)

    collection = pvd.createElementNS("VTK", "Collection")
    pvd_root.appendChild(collection)

    for i in range(len(list_pvtr)):
        dataSet = pvd.createElementNS("VTK", "DataSet")
        dataSet.setAttribute("timestep", str(list_cycle[i]))
        dataSet.setAttribute("group", "")
        dataSet.setAttribute("part", "0")
        dataSet.setAttribute("file", list_pvtr[i])
        collection.appendChild(dataSet)

    try:
        output_pvd_id = open(pvd_name, 'w')
        pvd.writexml(output_pvd_id, newl='\n')
        output_pvd_id.close()
    except:
        print('Error writing PVD file:', pvd_name)


def createVTRCoordinates(nxyz, dxyz, ncomponents=3, bases=[]):
    """ Create the coordinates for a VTR file

    Parameters:
        nxyz - [10, 12, 13] number of points in each dimension
        dxyz - [0.5, 0.5, 0.5] grid step in each dimension

    Keywords:
        ncomponents - number of dimensions
        bases - beginning of your coordinates

    Returns:
        vtk_coords, coords

    Usage:
        vtk_coords, coords = vtk_utils.createVTRCoordinates(dimensions, dxyz)
        vtk_utils.writeVTR2(OutFile, {'B': B, 'E': E}, {}, vtk_coords)

    Note, keep the "coords" array in order to correctly write the VTK files!

    """
    if not bases:
        bases = [0. for i in range(ncomponents)]

    coords = [np.arange(nxyz[i]).flatten().astype('float32') * dxyz[i] + bases[i] for i in range(ncomponents)]

    vtk_coords = []
    for i in range(ncomponents):
        vtk_coords.append(vtk.vtkFloatArray())
        vtk_coords[-1].SetNumberOfComponents(1)
        vtk_coords[-1].SetNumberOfTuples(nxyz[i])
        vtk_coords[-1].SetVoidArray(coords[i], nxyz[i], 1)

    return vtk_coords, coords

def createVTRCoordinatesIrregular(xyz,):
    """ Create the coordinates for a VTR file

    Parameters:
        xyz - list of 3 1D arrays [[x], [y], [z]] with grid coordinates in each dimension

    Returns:
        vtk_coords, coords

    Usage:
        vtk_coords, coords = vtk_utils.createVTRCoordinatesIrregular(xyz)
        vtk_utils.writeVTR2(OutFile, {'B': B, 'E': E}, {}, vtk_coords)

    Note, keep the "coords" array in order to correctly write the VTK files!

    """
    coords = [x.flatten().astype('float32').copy(order='C') for x in xyz]
    vtk_coords = []
    nxyz = [len(x) for x in coords]
    for i in range(len(coords)):
        vtk_coords.append(vtk.vtkFloatArray())
        vtk_coords[-1].SetNumberOfComponents(1)
        vtk_coords[-1].SetNumberOfTuples(nxyz[i])
        vtk_coords[-1].SetVoidArray(coords[i], nxyz[i], 1)

    return vtk_coords, coords

def writePVTR(pvtr_name, vtr_prefix, scalar_fields, vector_fields, ncxyz, numproc=1):
    """Writes the Parallel VTR file that contains info about particular VTR files
       NOTE, this routine implicitly assumes 3 dimensions!

       Parameters:
        pvtr_name - name of the output PVTR file
        vtr_prefix - prefix for VTR file names, e.g. 'fields00000000'
        scalar_fields - list of names of scalar fields in the VTR. May be empty!
        vector_fields - list of names of vector fields in the VTR. May be empty!
        ncxyz - number of grid cells in each dimension.
        numproc - number of processors (corresponds to the number of VTR files)

    """
    pvtr = xml.dom.minidom.Document()
    pvtr_root = pvtr.createElementNS("VTK", "VTKFile")
    pvtr_root.setAttribute("type", "PRectilinearGrid")
    pvtr_root.setAttribute("version", "0.1")
    pvtr_root.setAttribute("byte_order", "LittleEndian")
    pvtr.appendChild(pvtr_root)

    grid = pvtr.createElementNS("VTK", "PRectilinearGrid")
    grid.setAttribute("WholeExtent", "0 " + str(ncxyz[0]) + " 0 " + str(ncxyz[1]) + " 0 " + str(ncxyz[2]))
    grid.setAttribute("GhostLevel","0")
    pvtr_root.appendChild(grid)

    coordinate = pvtr.createElementNS("VTK", "PCoordinates")
    for k in range(3):
        data_array = pvtr.createElementNS("VTK", "PDataArray")
        data_array.setAttribute("type", "Float32")
        data_array.setAttribute("format", "Appended")
        coordinate.appendChild(data_array)

    grid.appendChild(coordinate)

    p_data = pvtr.createElementNS("VTK", "PPointData")
    if len(scalar_fields) > 0: p_data.setAttribute("Scalars", scalar_fields[0])
    if len(vector_fields) > 0: p_data.setAttribute("Vectors", vector_fields[0])
    for c in scalar_fields:
        data_array = pvtr.createElementNS("VTK", "PDataArray")
        data_array.setAttribute("type", "Float32")
        data_array.setAttribute("Name", c)
        p_data.appendChild(data_array)
    for c in vector_fields:
        data_array = pvtr.createElementNS("VTK", "PDataArray")
        data_array.setAttribute("type", "Float32")
        data_array.setAttribute("NumberOfComponents", "3")
        data_array.setAttribute("Name", c)
        p_data.appendChild(data_array)

    grid.appendChild(p_data)

    for i in range(numproc):
        vtr_name = vtr_prefix + "-" + str(i) + ".vtr"
        piece = pvtr.createElementNS("VTK", "Piece")
        piece.setAttribute("Extent", "0 " + str(ncxyz[0]) + " 0 " + str(ncxyz[1]) + " " + str(i*ncxyz[2]/numproc) + " " + str((i+1)*ncxyz[2]/numproc))
        piece.setAttribute("Source", vtr_name)
        grid.appendChild(piece)

    with open(pvtr_name, 'w') as pvtr_id:
        pvtr.writexml(pvtr_id, addindent='\t', newl='\n')


def writeVTR(vtr_name, scalar_fields, vector_fields, vtk_coords, rankproc=0, numproc=1):
    """Writes a single VTR file per Python processor/variable

       Parameters:
        vtr_name - name of the VTR file
        scalar_fields - dictionary with scalar field arrays ordered [x, y, z], e.g. {'p': array[nx,ny,nz], 'rho0': array[nx,ny,nz]}
        vector_fields - dictionary with vector fields ordered [3, x, y, z], e.g. {'J': array[3,nx,ny,nz], 'B': array[3,nx,ny,nz]}
        vtk_coords - VTR coordinates, see createVtrCoordinates()
        rankproc - rank of the processor for parallel output, set to 0 on serial
        numproc - number of processors, set to 1 on serial

    """
    nxyz = [vtk_coords[i].GetNumberOfTuples() - 1 for i in range(3)]
    numpoints = (nxyz[0]+1) * (nxyz[1]+1) * (nxyz[2] // numproc + 1)
    rtg = vtk.vtkRectilinearGrid()
    rtg.SetExtent(0, nxyz[0], 0, nxyz[1], rankproc*nxyz[2] // numproc, (rankproc+1)*nxyz[2] // numproc)
    rtg.SetXCoordinates(vtk_coords[0])
    rtg.SetYCoordinates(vtk_coords[1])
    rtg.SetZCoordinates(vtk_coords[2])
    vtk_data = []
    array_list = []
    for f in scalar_fields:
        vtk_data.append(vtk.vtkFloatArray())
        vtk_data[-1].SetNumberOfTuples(numpoints)
        vtk_data[-1].SetNumberOfComponents(1)
        array_list.append(scalar_fields[f].swapaxes(0,2).flatten().astype(np.float32))
        vtk_data[-1].SetVoidArray(array_list[-1], numpoints, 1)
        vtk_data[-1].SetName(f)
        if rtg.GetPointData().GetScalars():
            rtg.GetPointData().AddArray(vtk_data[-1])
        else:
            rtg.GetPointData().SetScalars(vtk_data[-1])

    for f in vector_fields:
        vtk_data.append(vtk.vtkFloatArray())
        vtk_data[-1].SetNumberOfTuples(numpoints*3)
        vtk_data[-1].SetNumberOfComponents(3)
        array_list.append(vector_fields[f].swapaxes(0,3).swapaxes(1,2).flatten().astype(np.float32))
        vtk_data[-1].SetVoidArray(array_list[-1], numpoints*3, 1)
        vtk_data[-1].SetName(f)
        if rtg.GetPointData().GetVectors():
            rtg.GetPointData().AddArray(vtk_data[-1])
        else:
            rtg.GetPointData().SetVectors(vtk_data[-1])

    try:
        writer = vtk.vtkXMLRectilinearGridWriter()
        writer.SetFileName(vtr_name)
        writer.SetInputData(rtg)
        writer.Write()
    except:
        print('Error writing VTR file:', vtr_name)


def writeVTR2(vtr_name, node_fields, cell_fields, VTKCoords):
    """ Writes a single VTR file per Python processor/variable.
    New version of writeVTR() as of 2018. Should replace the old one.

    Parameters:
        vtr_name - name of the VTR file
        node_fields - dictionary with vector fields ordered [3, x, y, z], e.g. {'J': array[3,nx,ny,nz], 'rho': array[1,nx,ny,nz]}

    """
    # Find out who's vector and who's scalar.
    scalar_names = []
    vector_names = []
    for k, v in node_fields.items():
        if (v.ndim == 3) or ((v.ndim == 4) and (v.shape[0] == 1)):
            scalar_names.append(k)
        else:
            vector_names.append(k)

    nxyz = node_fields[scalar_names[0]].shape if len(scalar_names) > 0 else node_fields[vector_names[0]].shape[1:]
    nxyz = np.array(nxyz)

    numpoints = nxyz.prod()

    rtg = vtk.vtkRectilinearGrid()
    rtg.SetExtent(0, nxyz[0]-1, 0, nxyz[1]-1, 0, nxyz[2]-1)
    rtg.SetXCoordinates(VTKCoords[0])
    rtg.SetYCoordinates(VTKCoords[1])
    rtg.SetZCoordinates(VTKCoords[2])

    vtk_data = []
    for name in scalar_names:
        vtk_data.append(vtk.vtkFloatArray())
        vtk_data[-1].SetNumberOfTuples(numpoints)
        vtk_data[-1].SetNumberOfComponents(1)
        node_fields[name] = node_fields[name].swapaxes(0, 2).flatten().astype(np.float32)
        vtk_data[-1].SetVoidArray(node_fields[name], numpoints, 1)
        vtk_data[-1].SetName(name)
        # If the first scalar was already set, use AddArray(). Otherwise, use SetScalars()
        if rtg.GetPointData().GetScalars() is not None:
            rtg.GetPointData().AddArray(vtk_data[-1])
        else:
            rtg.GetPointData().SetScalars(vtk_data[-1])

    for name in vector_names:
        vtk_data.append(vtk.vtkFloatArray())
        vtk_data[-1].SetNumberOfTuples(numpoints*3)
        vtk_data[-1].SetNumberOfComponents(3)
        node_fields[name] = node_fields[name].swapaxes(0,3).swapaxes(1,2).flatten().astype(np.float32)
        vtk_data[-1].SetVoidArray(node_fields[name], numpoints*3, 1)
        vtk_data[-1].SetName(name)
        # If the first vector was already set...
        if rtg.GetPointData().GetVectors():
            rtg.GetPointData().AddArray(vtk_data[-1])
        else:
            rtg.GetPointData().SetVectors(vtk_data[-1])

    writer = vtk.vtkXMLRectilinearGridWriter()
    writer.SetFileName(vtr_name)
    print('Writing VTR file', writer.GetFileName())
    if (vtk.vtkVersion.GetVTKMajorVersion() < 6):
        writer.SetInput(rtg)
    else:
        writer.SetInputData(rtg)
    writer.Write()


def readVTR(fname, Coords=False, Arrays=[]):
    """Read a single snapshot out of vtr file produced by proc2vtr.py

       Parameters:
         fname: vtr file name
         Coords=False: set it to True to return coordinates
         Arrays: names of the arrays to be read; all by default.

       Output:
          arrays with elements ordered [z,y,x,3] for vectors or [z,y,x,1] for scalars
          (optionally) A tuple with X, Y, Z coordinates

    """
    if not os.path.exists(fname):
        raise IOError('File does not exist: ' + fname)
    reader = vtk.vtkXMLRectilinearGridReader()
    reader.SetFileName(fname)
    reader.Update()
    rtg = reader.GetOutputAsDataSet()
    pts = rtg.GetPointData()
    arr_shape = (rtg.GetExtent()[5]-rtg.GetExtent()[4]+1, rtg.GetExtent()[3]-rtg.GetExtent()[2]+1, rtg.GetExtent()[1]-rtg.GetExtent()[0]+1)
    res = {}
    try:
        for i in range(pts.GetNumberOfArrays()):
            name = pts.GetArrayName(i)
            if (Arrays and (name in Arrays)) or (not Arrays):
                vtk_arr = pts.GetArray(i)
                arr = numpy_support.vtk_to_numpy(vtk_arr)
                res[name] = arr.reshape((arr_shape[0], arr_shape[1], arr_shape[2], vtk_arr.GetNumberOfComponents()))
    except:
        raise ValueError('Error reading array ' + name + ' from file ' + fname)

    if Coords:
        return res, (numpy_support.vtk_to_numpy(rtg.GetXCoordinates()), numpy_support.vtk_to_numpy(rtg.GetYCoordinates()), numpy_support.vtk_to_numpy(rtg.GetZCoordinates()))
    else:
        return res


def writeVTU(filename, scalar_data, vector_data, coords):
    """Saves unstructured point data to VTU file

       Parameters:
        scalar_data - dictionary with scalar field arrays, for each point e.g. {'color': array[N], 'Type': array[N]}
        vector_data - dictionary {'vec1': list of N arrays with 3 components, i.e., [N][3]}
        coords - list [N][3] with X, Y, Z coordinate for N points in the unstructured grid

    """
    # Number of points
    Npts = len(coords)

    # Create a dictionary with vtk arrays containing additional attributes for points
    vtk_point_data = {}
    for k in scalar_data.keys():
        # Append to the list of additional attributes
        vtk_point_data[k] = vtk.vtkFloatArray()
        vtk_point_data[k].SetNumberOfComponents(1)
        vtk_point_data[k].SetNumberOfTuples(Npts)
        vtk_point_data[k].SetName(k)

    # vectors
    for k in vector_data.keys():
        # Append to the list of additional attributes
        vtk_point_data[k] = vtk.vtkFloatArray()
        vtk_point_data[k].SetNumberOfComponents(3)
        vtk_point_data[k].SetNumberOfTuples(Npts)
        vtk_point_data[k].SetName(k)

    # Add points one-by-one
    points = vtk.vtkPoints()
    for i in range(Npts):
        # Coordinates
        points.InsertNextPoint(*coords[i])
        # scalars
        for k, v in scalar_data.items():
            vtk_point_data[k].SetTuple1(i, v[i])
        # vectors
        for k, v in vector_data.items():
            vtk_point_data[k].SetTuple3(i, *v[i])


    # Now set grid's point data
    ugrid = vtk.vtkUnstructuredGrid()
    ugrid.SetPoints(points)
    i = 0
    for k, v in vtk_point_data.items():
        if i == 0:
            if k in scalar_data.keys():
                ugrid.GetPointData().SetScalars(v)
            else:
                ugrid.GetPointData().SetVectors(v)
        else:
            ugrid.GetPointData().AddArray(v)
        i += 1

    writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetFileName(filename)
    writer.SetInputData(ugrid)
    writer.Write()


def parseExtent(s):
    """ Parses extent string in form:
         '0 64 0 64 0 32'
    into a numeric array
    """
    s = s.split(' ')
    res = []
    for ss in s:
        res.append(int(ss))
    res = np.array(res)
    if res.shape[0] != 6:
        print('Warning, array extent has strange number of elements: ', res.shape, '!')
    return res


def readPVTR(filename, Arrays = [], path = None, Coords=False):
    """ Reads a Parallel VTR file

        Parameters:
          filename - pvtr file name

        Keywords:
          Arrays: names of the arrays to be read; all by default.
          path - path to the files, if omitted, the path is extracted from the fname
          Coords=False: set it to True to return coordinates

        Output:
          arrays with elements ordered [z,y,x,3] for vectors or [z,y,x,1] for scalars
          (optionally) a tuple with X, Y, Z coordinates

    """
    if path is None:
        path, fname = os.path.split(filename)
        xmlfilename = filename
    else:
        xmlfilename = os.path.join(path, filename)

    with open(xmlfilename, 'r') as xmlfile:
        doc = xml.dom.minidom.parse(xmlfile)

    ## Get domain extent
    e = doc.getElementsByTagName('PRectilinearGrid')[0]
    whole_extent = parseExtent(e.getAttribute('WholeExtent'))

    ## Set up the shapes for all arrays and vectors
    ePointData = doc.getElementsByTagName('PPointData')[0]
    data = {}
    for e in ePointData.getElementsByTagName('PDataArray'):
        if (e.getAttribute('Name') in Arrays) or (not Arrays):
            data[e.getAttribute('Name')] = np.zeros([whole_extent[5]-whole_extent[4]+1, whole_extent[3]-whole_extent[2]+1, whole_extent[1]-whole_extent[0]+1,
                (lambda b: b and int(e.getAttribute('NumberOfComponents')) or 1)(e.hasAttribute('NumberOfComponents'))])

    if Coords:
        XYZ = (np.zeros(whole_extent[1]-whole_extent[0]+1), np.zeros(whole_extent[3]-whole_extent[2]+1), np.zeros(whole_extent[5]-whole_extent[4]+1))

    ## Read data pieces
    ePieces = doc.getElementsByTagName('Piece')
    for piece in ePieces:
        pe = parseExtent(piece.getAttribute('Extent'))

        # Read the data from a VTR file
        vtr_data, xyz = readVTR(os.path.join(path, piece.getAttribute('Source')), Arrays=Arrays, Coords=True)

        # Add piece's coordinates
        if Coords:
            for i in range(3):
                XYZ[i][pe[2*i]:pe[2*i+1]+1] = xyz[i]

        # Fill in the corresponding slab in the data array
        for a in vtr_data:
            data[a][pe[4]:pe[5]+1, pe[2]:pe[3]+1, pe[0]:pe[1]+1, :] = vtr_data[a]

    if Coords:
        return data, XYZ
    else:
        return data


def vtr2mat(filename, keys=None, ranges=None):
    """ Converts a VTR/PVTR to a MATLAB file.
    Parameters:
        specify keys[] that you want to save; others will be deleted
        ranges - a list of slices to cut from every array. Useful if you want to store only a part of the snapshot

    """
    name, ext = filename.split('.')
    if ext.lower() == 'pvtr':
        vtr_data = readPVTR(filename)
    elif ext.lower() == 'vtr':
        vtr_data = readVTR(filename)
    else:
        raise Exception('Wrong input extension: ' + ext)

    if keys is not None:
        for k in vtr_data.keys():
            if k not in keys:
                del(vtr_data[k])
            else:
                vtr_data[k] = vtr_data[k].swapaxes(0,2) # [x,y,z,1 or 3]

    if ranges:
        for k in vtr_data.keys():
            vtr_data[k] = vtr_data[k][ranges[0], ranges[1], ranges[2],:]

    scipy.io.savemat(name + '.mat', vtr_data)


def vtk2bin(filename, ArrayNames, ndim, output_filename=''):
    """ Converts a VTK/VTR to a Fortran unformatted binary file.

    Parameters:
        filename - full path to the VTK file
        ArrayNames - list of array names to be read
        ndim - number of dimensions in the array (1 - scalar, 3 - vector)

    """
    data, dxyz = read_ipic_vtk(filename, ArrayNames, ndim=ndim)

    print('Read ', filename)

    name = output_filename if output_filename else filename.split('.')[0]
    f = FortranFile(name + '.bin', 'w')
    for k, v in data.items():
        print('Processing ', k, ', Number of components: ', v.shape[3])
        for i in range(v.shape[3]):
            f.write_record(v[:, :, :, i])
    f.close
    print('Written file ', name + '.bin')


def vtk2dat(filename, ArrayNames, ndim, output_filename=''):
    """ Converts a VTK/VTR to a binary file using np.tofile.
    This routine to be used to pass data to Clare and Ben.

    Parameters:
        filename - full path to the VTK file
        ArrayNames - list of array names to be read
        ndim - number of dimensions in the array (1 - scalar, 3 - vector)

    """
    # data is shaped [x,y,z,3] or [x,y,z,1]
    data, dxyz = read_ipic_vtk(filename, ArrayNames, ndim=ndim)

    print('Read ', filename)

    name = output_filename if output_filename else filename.split('.')[0]
    name += '.dat'
    with open(name, 'wb') as f:
        for k, v in data.items():
            np.array([v.shape[:-1]], dtype='int32').tofile(f)
            print('Processing ', k, ', Number of components: ', v.shape[3])
            for i in range(v.shape[3]):
                v[:, :, :, i].astype('float32').T.tofile(f)
    print('Written file ', name)


def readVTK(filename, ArrayNames, ndim=3):
    """ Read a legacy, ASCII or binary, VTK file.

        Parameters:
            infile - name of the input (legacy VTK file)
            ArrayNames - list of the array names to be read, e.g., ['B', 'E']
            Don't forget to specify ndim for scalars!

        Return:
            A dictionary with arrays
            A tuple with coordinates

    """
    print('Reading VTK file ', filename)

    if not os.path.exists(filename):
        raise IOError('File does not exist: ' + filename)

    reader = vtk.vtkStructuredPointsReader()
    reader.SetFileName(filename)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data = reader.GetOutput()

    dim = data.GetDimensions()
    vec = list(dim)
    vec = [i for i in dim]  # TODO: remove this line?
    vec.append(ndim)

    Arrays = {}
    pdata = data.GetPointData()
    for name in ArrayNames:
        print('Processing array ', name)
        try:
            Arrays[name] = numpy_support.vtk_to_numpy(pdata.GetArray(name))
        except AttributeError:
            raise(ValueError('There is not array with name ' + name + ' in file ' + filename))

        try:
            Arrays[name] = Arrays[name].reshape(vec, order='F')
        except ValueError:
            raise(ValueError('Can not reshape array ' + name + ' from file ' + filename + '. Maybe the number of dimensions is wrong (ndim)?'))

    x = np.zeros(data.GetNumberOfPoints())
    y = np.zeros(data.GetNumberOfPoints())
    z = np.zeros(data.GetNumberOfPoints())

    for i in range(data.GetNumberOfPoints()):
        x[i], y[i], z[i] = data.GetPoint(i)

    x = x.reshape(dim, order='F')
    y = y.reshape(dim, order='F')
    z = z.reshape(dim, order='F')

    return Arrays, (x, y, z)


def read_ipic_vtk(filename, ArrayNames, return_coords=False, ndim=3):
    """ This is the preferred way to read all 3D vtk/vtr/pvtr files produced by iPic3D or data conversion scripts.
        Detect the type of the reader needed depending on file's extension, and read the necessary arrays.
        NOTE, assumes 3D arrays only!

        Parameters:
            filename - name of the file to read, e.g. 'C:/Temp/B.pvtr' or '/home/gates/E00001000.vtk'
            ArrayNames - list of data array names to be extracted, e.g., ['B', 'E', ]

        Keywords:
            return_coords - if True returns point coordinates, otherwise returns only dx, dy, dz.
            ndim - 1 for scalars and 3 for vectors. Only used for reading legacy VTK files.

        Returns a tuple of 2 elements:
            1. dictionary of arrays ordered [x, y, z, ndim] where ndim is 1 for scalars (rho, EJ) or 3 for vectors (B, J, E)
            2. [dx, dy, dz] (or coordinates)

    """
    print('Reading arrays ', ArrayNames, ' from VTK file ', filename)

    name, ext = os.path.splitext(filename)
    if ext.startswith('.'):
        ext = ext[1:]
    ext = ext.lower()

    if ext == 'vtk':
        arrs, xyz = readVTK(filename, ArrayNames, ndim=ndim)
        if xyz[0].ndim == 3:
            dxyz = [xyz[0][1, 0, 0] - xyz[0][0, 0, 0], xyz[1][0, 1, 0] - xyz[1][0, 0, 0], xyz[2][0, 0, 1] - xyz[2][0, 0, 0]]
        elif xyz[0].ndim == 1:
            dxyz = [x[1] - x[0] for x in xyz]

    elif ext in ['vtr', 'pvtr']:
        if ext == 'pvtr':
            arrs, xyz = readPVTR(filename, Arrays = ArrayNames, Coords=True)
        elif ext == 'vtr':
            arrs, xyz = readVTR(filename, Arrays=ArrayNames, Coords=True)

        dxyz = [x[1] - x[0] for x in xyz]
        # Change order from [z, y, x, 3] to [x,y,z,3] (or [x,y,z,1] for a scalar)
        for k in arrs:
            arrs[k] = arrs[k].swapaxes(0, 2)

    else:
        raise ValueError('Only vtk, vtr, pvtr formats are supported!')

    if return_coords:
        return arrs, xyz
    else:
        return arrs, dxyz


class iPicPVTRReader:
    """ Interface to read PVTR and be able to get only slices.

        USAGE:

    """
    def __init__(self):
        self.FileName = ''
        self.Path = ''
        self.parts = []
        self.WholeExtent = []
        self.Arrays = []
        self.PVTR = None


    def parse(self, path):
        """ Reads only the XML content of the PVTR, the geometry

            Parameters:
                path - full file name of the PVTR file
                Coords=False: set it to True to return coordinates

            Output:
                arrays with elements ordered [z,y,x,3] for vectors or [z,y,x,1] for scalars
                (optionally) A tuple with X, Y, Z coordinates
        """
        self.Path, self.FileName = os.path.split(path)
        with open(path, 'r') as xmlfile:
            self.PVTR = xml.dom.minidom.parse(xmlfile)

        ## Get domain extent
        e = self.PVTR.getElementsByTagName('PRectilinearGrid')[0]
        self.WholeExtent = parseExtent(e.getAttribute('WholeExtent'))

        ## Set up the shapes for all arrays and vectors
        ePointData = self.PVTR.getElementsByTagName('PPointData')[0]
        self.Arrays = []
        for e in ePointData.getElementsByTagName('PDataArray'):
            self.Arrays.append({'Name': e.getAttribute('Name'),
                                'XMLElement': e,
                                'NumberOfComponents': (lambda b: b and int(e.getAttribute('NumberOfComponents')) or 1)(e.hasAttribute('NumberOfComponents'))})
        ## Data pieces
        ePieces = self.PVTR.getElementsByTagName('Piece')
        for piece in ePieces:
            self.parts.append({'Extent': parseExtent(piece.getAttribute('Extent')),
                               'FileName': os.path.join(self.Path, piece.getAttribute('Source'))})


    def ReadPart(self, index, Coords=False, Arrays=[]):
        """ Read the desired part from the PVTR file """
        if (index < 0) or (index > len(self.parts)):
            print('Part index out of bounds!')
            return None
        if not Arrays:
            Arrays = [a['Name'] for a in self.Arrays]
        # We have to change the coordinate order so Z is the last...
        return readVTR(self.parts[index]['FileName'], Coords=Coords, Arrays=Arrays)


def import_colormap():
    """TODO: this is received from Jan Deca on 29.07.2014
    Optimize it and make usable
    """
    cmap_paraview = [0,0,0.278431,0.278431,0.858824,0.143,0,0,0.360784,0,1,1,0,0.501961,0,1,1,0,1,0.380392,0,0.419608,0,0,0.878431,0.301961,0.301961]
    cmap_paraview = [0,0,0,0.278431,0.858824,0.143,0,0,0.360784,0,1,1,0,0.501961,0,1,1,0,1,0.380392,0,0.419608,0,0,0.878431,0.301961,0.301961,1,1,1]
    ctmp = []
    i=0
    while i < len(cmap_paraview):
        ctmp.append([cmap_paraview[i], cmap_paraview[i+1], cmap_paraview[i+2]])
        i+=3
    paracm=LinearSegmentedColormap.from_list('test',ctmp,N=1024,gamma=1.0)