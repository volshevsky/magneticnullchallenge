""" Find magnetic null-points by computing Poincare indices.
    Translation of Huishan Fu's MATLAB routines.

    The method is explained in:
        Kepko, Khurana, Kivelson, Elphic and Russel, IEEE Transactions on Magnetism, 1996.
        Khurana, Kepko, Kivelson & Elphic, IEEE Transactions on Magnetism, 1996.

    class NullPointFinder:
        findNullsTetra() - feed it a 3D array of vectors (B field) to locate nulls.
        findNullsHexa() - feed it a 3D array of vectors (B field) to locate nulls.
        writeVTU() - saves nulls to the VTU file readable by ParaView.
        writeCSV() - saves nulls to a CSV file.

    (c) Vyacheslav Olshevsky 2013-2016
    email: sya@mao.kiev.ua

"""
import os
import csv
import time
import numpy as np
import pylab as plt
from glob import glob

import vtk_utils

small_double = 1e-20
truncate_imag = 1e-6
truncate_real = 1. / truncate_imag


def solid_angle(R, normalized=1):
    """ Calculate the solid angle of three vectors in a unit sphere, normalized by 4pi, i.e., the fraction of the sphere's surface.

        Parameters:
            R - array of size [3,:,3] - [r1, r2, r3] where ri has shape [:,3] or [:,{xyz}]
        Keywords:
            normalized - set this to False if r1, r2, r3 are not normalized to unity

        Math following A. VAN OOSTEROM AND J. STRACKEE, The Solid Angle of a Plane Triangle,
        IEEE TRANSACTIONS ON BIOMEDICAL ENGINEERING, VOL. BME-30, NO. 2, FEBRUARY 1983

    """
    numerator = (R[0] * np.cross(R[1], R[2], axis=1)).sum(axis=1)

    if not normalized:
        Rl = (R**2).sum(axis=-1)**0.5
    else:
        Rl = np.ones((3,), dtype=R.dtype)

    denominator = Rl[0] * Rl[1] * Rl[2] \
                + Rl[2] * (R[0]*R[1]).sum(axis=1) \
                + Rl[1] * (R[0]*R[2]).sum(axis=1) \
                + Rl[0] * (R[1]*R[2]).sum(axis=1)

    # Normalize the result to 4pi
    return 0.5 / np.pi * np.arctan2(numerator, denominator)

def is_point_enclosed(p, vertices):
    """Check whether given point(s) is enclosed in the tetrahedron.
    Compute the sum of solid angles between all triplets of vectors from
    the point to the vertices.

    Parameters:
        p - [3]
        vertices - [4,?,3]

    """
    triplets = [[0, 1, 2], [0, 3, 1], [0, 2, 3], [1, 3, 2]]
    vectors = vertices - p
    area = np.zeros(vectors.shape[1])
    for t in triplets:
        area += solid_angle(vectors[t], normalized=0)
    return area

def poincare_index(vec1, vec2, vec3, vec4, SmallFloat=1e-10):
    """ Compute Poincare index for a tetrahedron.

        Parameters:
            vec1, vec2, vec3, vec4 - [:, 3] vectors of magnetic field at the vertices.
            Where the magnetic field is exactly zero, returns 1.

        The original MATLAB c_fgm_poincare_index.m by:
            Yunhui Hu, Dec.20.2006 in Wuhan,
            modified by Shiyong Huang, May.17.2012 in IRF

    """
    Map_sc = np.ndarray((4, vec1.shape[0], vec1.shape[1]))
    infinitesimal = np.zeros(vec1.shape[0], dtype=np.int32)

    # Project from configuration (x, y, z) space to Magnetic field space (Bx, By, Bz)
    for k, v in enumerate([vec1, vec2, vec3, vec4]):
        Map_sc[k,...] = v / ((v**2).sum(axis=1, keepdims=True)**0.5 + SmallFloat)
        infinitesimal += ((Map_sc[k]**2).sum(axis=1) < SmallFloat)

    # The result is the sum of the solid angles for each triangle
    area = np.zeros(vec1.shape[0])
    for t in [[0, 1, 2], [0, 3, 1], [0, 2, 3], [1, 3, 2]]:
        area += solid_angle(Map_sc[t])

    # In the points where |B| is zero, set area (Poincare index) to 1, which signalls a null.
    area[infinitesimal > 0] = 1

    return area

def preselect_grid_cells(B):
    """ Given a 4D array [x,y,z,3] of magnetic field measurements at computational grid nodes,
    choose only the locations where at least one field component flips sign.

    This shall work only if spacecraft separation == 1 grid cell

    Returns:
        3D array with 0 (no null in this cell) and 1 (possibly, there is a null in this cell).
        The shape of this array is 1 smaller than the shape of each component of B, i.e., [x-1,y-1,z-1]

    """
    sl = slice(None, -1)
    sr = slice(1, None)
    vertices = [(sl, sl, sl), (sl, sl, sr), (sl, sr, sr), (sl, sr, sl), (sr, sl, sl), (sr, sl, sr), (sr, sr, sr), (sr, sr, sl), ]

    def reduce_sign(a, v):
        """ Sum multiple views of the same 3D array a.
        a - 3D array
        v - list of vertices
        """
        lv = len(v)
        if lv == 2:
            return a[v[0]] + a[v[1]]
        elif lv == 1:
            return a[v[0]]
        else:
            return reduce_sign(a, v[: lv // 2]) + reduce_sign(a, v[lv // 2:])

    selected_cells = np.ones([n - 1 for n in B.shape[:-1]], dtype=np.int32)

    # Cycle over Bx, By, Bz.
    # Sum the signs of the field component over 8 nodes of each cell.
    # If the component doesn't flip sign in all of them,
    # the field can't be zero inside the cell in a linear approximation.
    for c in range(B.shape[-1]):
        S = np.sign(B[:, :, :, c])
        selected_cells *= (np.abs(reduce_sign(S, vertices)) < 8)

    #print('Selected %d cells out of %d' % (selected_cells.sum(), np.prod(B.shape[:-1])))

    return selected_cells

def preselect_cells(B):
    """ Given a list of n 1D arrays of magnetic field measurements in a sequence of points (spacecraft positions),
    choose only the locations where all field components flip sign.

    Parameters:
        B - list of arrays with B field in each vertex [B1[:,2], B2[:,3], ..., Bn[:,3]]
            normally n == 4.

    Returns:
        indices of the tetrahedra, on the vertices of which all field components flip sign.

    """
    vtx_count = len(B)
    dim_count = B[0].ndim - 1
    selected_cells = np.ones([n for n in B[0].shape[:-1]], dtype=np.int32)

    # Cycle over Bx, By, Bz.
    # Sum the signs of the field component over all vertices.
    # If the component doesn't flip sign in all of them,
    # the field can't be zero inside the cell in a linear approximation.
    for c in range(B[0].shape[-1]):
        idcs = tuple([slice(None, None),] * dim_count + [c,])
        S = np.sign(B[0][idcs])
        for i in range(1, vtx_count):
            S += np.sign(B[i][idcs])
        selected_cells *= (np.abs(S) < vtx_count)

    #print('Selected %d cells out of %d' % (selected_cells.sum(), np.prod(B.shape[:-1])))

    return selected_cells

def poincare_index_hexa(F, SmallFloat=1e-6):
    """ Compute Poincare index for a hexahedron given 8 3D arrays with B field at each vertex.
    Let the vertices be indexed in the following order (from top to bottom, counterclockwise):
        top-front-left     - 0
        top-front-right    - 1
        top-back-right     - 2
        top-back-left      - 3
        bottom-front-left  - 4
        bottom-front-right - 5
        bottom-back-right  - 6
        bottom-back-left   - 7

    12 triangles which split the faces of the hexahedron (vertices in the counterclockwise order!):
        [0,4,5], [0,5,1], [5,2,1], [5,6,2], [2,6,7], [2,7,3], [7,0,3], [7,4,0], [7,5,4], [7,6,5], [0,1,2], [0,2,3]

    Parameters:
        F - list of 8 arrays of shape [nx*ny*nz, 3] of the (magnetic) field at the vertices of the hexahedron, ordered in the above way.

    Returns:
        array of shape [nx*ny*nz] - the area in the magnetic field space for each hexahedron (should be -1, 0, or 1?).
        Where |B| < SmallFloat, returns 1.

    """
    # 12 triangles which split the faces of each hexahedron
    triangles = [[0,4,5], [0,5,1], [5,2,1], [5,6,2], [2,6,7], [2,7,3], [7,0,3], [7,4,0], [7,5,4], [7,6,5], [0,1,2], [0,2,3]]
    # There will be one solid angle in the magnetic field space for each triangle.
    # Here is a matrix, mapping configuration space into the magnetic field space.
    F_norm = np.ndarray((len(F), F[0].shape[0], F[0].shape[1]))
    # Indices of the hexahedrons where null points are detected
    infinitesimal = np.zeros(F[0].shape[0], dtype=np.int32)

    # Project from configuration (x, y, z) space to Magnetic field space (Bx, By, Bz)
    # Normalize to the absolute value of magnetic field in each vertex
    for i, f in enumerate(F):
        F_norm[i, ...] = f / (((f**2).sum(axis=1, keepdims=True))**0.5 + SmallFloat)
        infinitesimal += ((F_norm[i]**2).sum(axis=1) < SmallFloat)

    # The result is the sum of the solid angles for each triangle
    area = np.zeros(F[0].shape[0])
    for t in triangles:
        area += solid_angle(F_norm[t])

    # In the points where |B| is zero, set area (Poincare index) to 1, which signalls a null.
    area[infinitesimal > 0] = 1

    return area

def field_gradient(vec1, vec2, vec3, vec4, B1, B2, B3, B4):
    """Compute a 9-gradient of the B-field in the four-spacecraft tetrahedron.

    Parameters:
        vec1, vec2, vec3, vec4 - vectors (numpy arrays of length 3) with spacecraft locations
        B1, B2, B3, B4 - magnetic fields measured by the four spacecraft

    Returns:
        3 x 3 gradient matrix: dBidxj = {dBi/dxj}

    Khurana, K. K. et al. Accurate determination of magnetic field gradients from four-point vector
    measurements-II: use of nutral constraints on vector data obtained from four spinning spacescraft.
    IEEE Trans. Magn. 32, 5193-5205 (1996).
    """
    A = np.array([vec2 - vec1, vec3 - vec1, vec4 - vec1])
    A = np.linalg.inv(A)
    B = np.array([B2 - B1, B3 - B1, B4 - B1])
    return np.dot(A, B)

def field_gradient_hexa(coords, F):
    """Compute a 9-gradient of the vector field measured in the vertices of a hexahedron.
    The vertices should be ordered as:
        top-front-left     - 0
        top-front-right    - 1
        top-back-right     - 2
        top-back-left      - 3
        bottom-front-left  - 4
        bottom-front-right - 5
        bottom-back-right  - 6
        bottom-back-left   - 7

    Parameters:
        coords - list of 8 vectors (numpy arrays [3]) with vertice locations
        F - list of 8 vectors (numpy arrays [3]) with field measurements

    Returns:
        3 x 3 gradient matrix

    """
    dFdx = 0.25 * ((F[1] - F[0])/(coords[1][0]-coords[0][0]) + (F[2] - F[3])/(coords[2][0]-coords[3][0]) + (F[6] - F[7])/(coords[6][0]-coords[7][0]) + (F[5] - F[4])/(coords[5][0]-coords[4][0]))
    dFdy = 0.25 * ((F[3] - F[0])/(coords[3][1]-coords[0][1]) + (F[2] - F[1])/(coords[2][1]-coords[1][1]) + (F[6] - F[5])/(coords[6][1]-coords[5][1]) + (F[7] - F[4])/(coords[7][1]-coords[4][1]))
    dFdz = 0.25 * ((F[0] - F[4])/(coords[0][2]-coords[4][2]) + (F[3] - F[7])/(coords[3][2]-coords[7][2]) + (F[2] - F[6])/(coords[2][2]-coords[6][2]) + (F[1] - F[5])/(coords[1][2]-coords[5][2]))
    return np.array([dFdx, dFdy, dFdz])

def null_point_type(e_vals, coords):
    """ Define null-point type depending on the dB/dr eigenvalues

    Parameters:
        e_vals - eigenvalues of the grad(B) tensor
        coords - coordinates of the null point (used only for error reporting)

    Description:
        Lau & Finn ApJ 350, 672 (1989):
        1) One eigenvalue vanishes (rare in 3D): k1 + k2 = 0, if k1, k2 are real this is an X-point, otherwise an O-point. Structurally unstable in 3D.
        2) k1, k2, k3 are all real: a "three-dimensional X-point". Signature (+--) defines type A null, (-++) defines a type B null.
        3) k1 real, k2, k3 complex: a "three-dimensional O-point". Sign of k1 defines positive/negative null of As (Bs) type. 
        If k1 > 0 the field spirals INTO the null, and there must be a current through the point.

    """
    # Try to truncate the small imaginary/real parts
    for i in range(len(e_vals)):
        e = e_vals[i]
        r = abs(e.imag / e.real)
        if r < truncate_imag:
            e_vals[i] = e.real
        elif r > truncate_real:
            e_vals[i] = complex(0., e.imag)

    if any(abs(e_vals) < small_double):
        print('Warning, zero eigenvalue detected at ', coords)
        if sum(np.isreal(e_vals)) >= 2:
            null_type = 'X'
        else:
            null_type = 'O'
    elif sum(np.isreal(e_vals)) == 3:
        s = sum(np.sign(e_vals))
        if s < 0:
            null_type = 'A'
        elif s > 0:
            null_type = 'B'
        else:
            print('Coordinates: ', coords, '\nSignature: ', np.sign(e_vals))
            raise Exception('Unknown signature in all-positive eigen values!')
    elif sum(np.isreal(e_vals)) == 1:
        s = e_vals[np.where(np.isreal(e_vals))][0]
        if s > 0:
            null_type = 'As'
        elif s < 0:
            null_type = 'Bs'
        else:
            print('Coordinates: ', coords, '\nValues: ', e_vals)
            raise Exception('Zero eigenvalue detected in three non-zero ones!')
    else:
        print('Coordinates: ', coords, '\nValues: ', e_vals)
        raise Exception('Wrong number of real eigen values!')
    return null_type

def null_location_secant(vec_arr, B_arr):
    """ Find null point location within a tetrahedron as described by Greene (1992)

        Parameters:
            vec_arr - an array of radius-vectors of the vertices, [v0, v1, v2, v3]; vec_arr[i,:] = vec_i
            B_arr - same for B vectors

    """
    if not np.array_equal(vec_arr.shape, [4, 3]):
        raise ValueError('Should supply four 3D vectors!')

    # find the lower left corner for convenience
    shifts = np.zeros(3) - 1 # indices of the vectors (in vec_arr) that are shifted from r0 by dx, dy, dz, corresponsingly
    ma = vec_arr.max(axis=0)
    mi = vec_arr.min(axis=0)
    for i in range(3):
        ima = np.where(vec_arr[:, i] == ma[i])[0]
        imi = np.where(vec_arr[:, i] == mi[i])[0]
        if (len(ima) == 3) and (len(imi) == 1):
            shifts[i] = imi
        elif (len(imi) == 3) and (len(ima) == 1):
            shifts[i] = ima
        else:
            raise ValueError('The tetrahedron vertices should be in the adjacent nodes of a rectilinear grid!\n' + str(vec_arr))

    # If everything is fine, we should get exactly 1 vector left.
    # Find the "cornerstone" by excluding the shifts
    i0 = [i for j, i in enumerate(range(4)) if j not in shifts]
    if len(i0) != 1:
        raise ValueError('Something went wrong, exactly 3 vectors should have been excluded! Shifts:' + str(shifts))
    r0 = vec_arr[i0].flatten()
    B0 = B_arr[i0].flatten()
    Bx, By, Bz = [B_arr[s,:].flatten() for s in shifts]
    dx, dy, dz = [float(vec_arr[shifts[i]][i] - r0[i]) for i in range(3)]

    r = np.zeros(3)
    r[0] = r0[0] + dx * np.dot(B0, np.cross(By, Bz)) / (np.dot(B0, np.cross(By, Bz)) + np.dot(B0, np.cross(Bz, Bx)) + np.dot(B0, np.cross(Bx, By)) - np.dot(Bx, np.cross(By, Bz)))
    r[1] = r0[1] + dy * np.dot(B0, np.cross(Bz, Bx)) / (np.dot(B0, np.cross(Bz, Bx)) + np.dot(B0, np.cross(Bx, By)) + np.dot(B0, np.cross(By, Bz)) - np.dot(By, np.cross(Bz, Bx)))
    r[2] = r0[2] + dz * np.dot(B0, np.cross(Bx, By)) / (np.dot(B0, np.cross(Bx, By)) + np.dot(B0, np.cross(By, Bz)) + np.dot(B0, np.cross(Bz, Bx)) - np.dot(By, np.cross(Bx, By)))

    return r

def null_location(vec_arr, B_arr):
    """ Find null point location within a tetrahedron as described by Greene (1992),
    or by just averaging the coordinates of the null enclosing tetra/hexahedron.

    Parameters:
        vec_arr - radius-vectors of the vertices
        B_arr - Magnetic fields at the vertices

    """
    # Greene (1992)
    deltaB = field_gradient(vec_arr[0, :], vec_arr[1, :], vec_arr[2, :], vec_arr[3, :], B_arr[0, :], B_arr[1, :], B_arr[2, :], B_arr[3, :])
    invB = np.linalg.inv(deltaB)
    r0 = [vec_arr[i, :] - np.dot(invB, B_arr[i, :]) for i in range(4)]
    return r0[0]

    # Simple averaging
    #return vec_arr.sum(axis=0) / np.float(vec_arr.shape[0])


class NullPointFinder:
    """ Allows to find nulls in a sequence of data cubes in different formats

    Properties:
        Tetrahedrons - the combinations of tetrahedrons in which to look for nulls.
        SpacecraftSeparation - the distance between the spacecraft in grid points

    """
    def __init__(self, input_path='', output_path='', input_prefix='', output_prefix='', indices=[], sc_dist=1, verbose=True):
        #
        # Nomenclature (following Huishan Fu):
        #    l=left    r=right
        #    f=front   b=back
        #    g=ground  c=ceiling
        #

        #
        # Stuff for locating nulls using tetrahedrons, to imitate Cluster/MMS spacecraft sonfiguration
        # Inside each hexahedron cell, there are five terehadrons:
        #    terehadron 1: (Brbg,Brfg,Blfg,Brfc);
        #    terehadron 2: (Blfg,Blbg,Brbg,Blbc);
        #    terehadron 3: (Blbc,Blfc,Brfc,Blfg);
        #    terehadron 4: (Blbc,Brbc,Brfc,Brbg);
        #    terehadron 5: (Blfg,Brbg,Brfc,Blbc);
        #
        self.Tetrahedrons = [['rbg', 'rfg', 'lfg', 'rfc'], ['lfg', 'lbg', 'rbg', 'lbc'], ['lbc', 'lfc', 'rfc', 'lfg'], ['lbc', 'rbc', 'rfc', 'rbg'], ['lfg', 'rbg', 'rfc', 'lbc']]
        self.sc_distance = sc_dist
        self.sc_location = {'l': slice(0, -sc_dist), 'r': slice(sc_dist, None), 'f': slice(0, -sc_dist), 'b': slice(sc_dist, None), 'g': slice(0, -sc_dist), 'c': slice(sc_dist, None)}
        self.sc_shift = {'l': 0, 'r': sc_dist, 'f': 0, 'b': sc_dist, 'g': 0, 'c': sc_dist}

        #
        # Stuff to locate nulls in hexahedrons.
        # The vertices are ordered from top to bottom. In the above nomenclature:
        # top-front-left     - 0 = lfc
        # top-front-right    - 1 = rfc
        # top-back-right     - 2 = rbc
        # top-back-left      - 3 = lbc
        # bottom-front-left  - 4 = lfg
        # bottom-front-right - 5 = rfg
        # bottom-back-right  - 6 = rbg
        # bottom-back-left   - 7 = lbg
        #
        self.HexaVertices = ['lfc', 'rfc', 'rbc', 'lbc', 'lfg', 'rfg', 'rbg', 'lbg']

        self.null_types = ['X', 'O', 'A', 'B', 'As', 'Bs']
        self.null_colors = {'X': 'lightblue', 'O': 'yellow', 'A': 'lightblue', 'B': 'blue', 'Bs': 'red', 'As': 'orange'}
        self.null_symbols = {'X': 'x', 'O': 'o', 'A': 'h', 'B': 'h', 'Bs': 's', 'As': 's'}
        self.null_colors_num = {'X': 1, 'A': 2, 'B': 3, 'Bs': 4, 'As': 5, 'O': 6}

        self.input_path = input_path
        self.input_file = ''
        self.output_path = output_path
        self.input_prefix = input_prefix
        self.output_prefix = output_prefix
        self.indices = indices

        self.dxyz = []
        self.verbose = verbose

    def findNullsTetra(self, B, xyz=None):
        """ Locate nulls given a 4D array of magnetic field (x, y, z, 3) given on a grid
        by splitting each grid cell into multiple tetrahedrons ('spacecraft configurations').

        Parameters:
            B - Array [x, y, z, 3] of magnetic field measurements.
            xyz - list of 1D arrays [[x], [y], [z]] with coordinates of each grid vertice.

        """
        idx_grid = [np.arange(s) for s in B.shape[:-1]]
        shape = np.array([i - self.sc_distance for i in B.shape[:-1]] + [B.shape[-1],])
        if xyz is None:
            xyz = [i.astype(np.float32) for i in idx_grid]

        res = []
        for t in self.Tetrahedrons:
            # The coordinates of the spacecraft: sc_xyz[spacecraft][dimension]
            sc_idcs = [[idx_grid[i][self.sc_location[s[i]]] for i in range(3)] for s in t]

            B_list = [B[self.sc_location[s[0]], self.sc_location[s[1]], self.sc_location[s[2]], :] for s in t]
            preselected_cells = np.where(preselect_cells(B_list) == 1)
            preselected_cells = np.array(preselected_cells).transpose()  # n rows, 3 columns

            # Construct a list of B values for the Poincare index
            #tetra_list = [b[preselected_cells] for b in B_list]
            tetra_list = [[] for i in range(len(B_list))]
            idx_list = [[] for i in range(len(B_list))]
            for c in preselected_cells:
                for i in range(len(tetra_list)):
                    # c is an array with [i,j,k] indices
                    tetra_list[i].append(B_list[i][tuple(c.tolist())])
                    idx_list[i].append([sc_idcs[i][j][c[j]] for j in range(3)])
            tetra_list = [np.array(tl) for tl in tetra_list]

            PI = poincare_index(*tetra_list)
            PI[np.where(np.isnan(PI))] = 0
            # tuple with 3 1D arrays of indices in each coordinate dimension. preselected_cells[nulls[0][i], nulls[0][j], nulls[0][i], ]
            nulls = np.where(np.abs(PI) > 0.9)[0]

            if self.verbose:
                print('Tetrahedron: ', t, '; Nulls found: ', nulls.shape[0])

            # null is an index in idx_list and B_list
            for null in nulls:
                # X0,Y0,Z0,Bx0,By0,Bz0,X1,Y1,Z1,Bx1,By1,Bz1,X2,Y2,Z2,Bx2,By2,Bz2,X3,Y3,Z3,Bx3,By3,Bz3,lambda1,lambda2,lambda3,type
                sc_coords = []  # Locations and B measurements
                B_fields = [] # B measurements at the spacecraft locations
                d = {}

                # For each spacecraft in the tetrahedron
                for sc in range(len(t)):
                    stri = repr(sc)
                    idcs = idx_list[sc][null]
                    cds = [xyz[i][idcs[i]] for i in range(3)]
                    d['B' + stri] = B[tuple(idcs)]
                    sc_coords.append(np.array(cds))
                    B_fields.append(np.array(B[tuple(idcs)]))

                # Find out the null type and gradB eigenvalues
                e_vals, _ = np.linalg.eig(field_gradient(*(sc_coords + B_fields)))
                d['xyz'] = null_location(np.array(sc_coords), np.array(B_fields))
                d['lambda'] = e_vals
                d['T'] = null_point_type(e_vals, d['xyz'])
                res.append(d)

        # Save nulls to nulls_found, sort them and return a _copy_ of nulls_found
        self.nulls_found = list(res)

        return res

    def findNullsTetraList(self, B, xyz):
        """ Locate nulls given magnetic field measurements and vertex coordinates.

        Parameters:
            B - magnetic field array (4, ?, 3)
            xyz - Coordinates of each vertice (4, ?, 3).

        """
        Nsc, _, _ = B.shape

        PI = poincare_index(*B)

        detected_nulls = np.where(np.abs(PI) > 0.9)[0]

        nulls_found = []
        # null is an index in idx_list and B_list
        for null in detected_nulls:
            sc_coords = []  # Locations and B measurements
            B_fields = [] # B measurements at the spacecraft locations
            d = {}

            for sc in range(Nsc):
                cds = xyz[sc][null,:]
                b_at_sc = B[sc][null,:]
                d['B' + repr(sc)] = b_at_sc
                sc_coords.append(cds)
                B_fields.append(b_at_sc)

            # Find out the null type and gradB eigenvalues
            e_vals, _ = np.linalg.eig(field_gradient(*(sc_coords + B_fields)))
            sc_coords = np.array(sc_coords)
            d['xyz'] = null_location(sc_coords, np.array(B_fields))
            d['lambda'] = e_vals
            d['T'] = null_point_type(e_vals, d['xyz'])

            # Additional fields for the null-finder challenge
            # Index of the point on the scacecraft trajectory
            d['point_index'] = null
            # Distance to the tetrahedron center
            center = sc_coords.sum(axis=0) / np.float(sc_coords.shape[0])
            d['dist'] = np.sum((center - d['xyz'])**2, axis=-1, keepdims=False)

            nulls_found.append(d)

        # Save detected_nulls to nulls_found, sort them and return a _copy_ of nulls_found
        self.nulls_found = list(nulls_found)

    def findNullsHexa(self, B, xyz=None):
        """ Locate nulls given a 4D array of magnetic field (x, y, z, 3), using hexahedron cells.

        Parameters:
            B - [x,y,z,3] - magnetic field given on the vertices of 3D grid
            xyz - list of 3 1D arrays [[x], [y], [z]] containing coordinates of each grid point (vertex)

        """
        # indices of vertices of the grid
        idx_grid = [np.arange(s) for s in B.shape[:-1]]
        shape = np.array([B.shape[0] - self.sc_distance, B.shape[1] - self.sc_distance, B.shape[2] - self.sc_distance, B.shape[3]])

        if xyz is None:
            xyz = [i.astype(np.float32) for i in idx_grid]

        res = []

        # Compute Poincare indices for all spacecraft locations
        if self.sc_distance == 1:
            preselected_cells = np.where(preselect_grid_cells(B) == 1)
            preselected_cells = np.array(preselected_cells).transpose()
            # List of 8 arrays with B field at cell vertices.
            # Each of the shape [N, 3] (or [3, N]??) where N is the number of pre-selected cells. 
            hexa_list = [[] for i in range(len(self.HexaVertices))]
            for c in preselected_cells:
                for i in range(len(self.HexaVertices)):
                    hv = self.HexaVertices[i]
                    vtx_idx = [idx_grid[j][c[j] + self.sc_shift[hv[j]]] for j in range(3)]
                    hexa_list[i].append(B[vtx_idx[0], vtx_idx[1], vtx_idx[2], :])
            hexa_list = [np.array(h) for h in hexa_list]
        else:
            # All cells are pre-selected in this case
            hexa_list = [B[self.sc_location[s[0]], self.sc_location[s[1]], self.sc_location[s[2]], :].reshape((shape[:-1].prod(), shape[-1])) for s in self.HexaVertices]

        PI = poincare_index_hexa(hexa_list)
        PI[np.where(np.isnan(PI))] = 0

        # A cell encloses an odd number of nulls (we say it is exactly 1 null) if its Poincare index == 1. 
        # In reality, it is better to check against a somewhat lower threshold, e.g., 0.9.
        if self.sc_distance == 1:
            # x, y, z indices of the cells enclosing nulls
            null_cells = preselected_cells[np.where(np.abs(PI) > 0.9)[0], :]
        else:
            PI = PI.reshape(shape[:-1])
            null_cells = np.array(np.where(np.abs(PI) > 0.9)).transpose()

        for null in null_cells:
            # X0,Y0,Z0,Bx0,By0,Bz0,X1,Y1,Z1,Bx1,By1,Bz1,X2,Y2,Z2,Bx2,By2,Bz2,X3,Y3,Z3,Bx3,By3,Bz3,lambda1,lambda2,lambda3,type
            vtx_coords = []  # Coordinates of the vertices enclosing the cell with null
            B_fields = [] # B field value in each vertex
            d = {}

            # For each vertex in the hexahedron
            for i in range(len(self.HexaVertices)):
                stri = repr(i)
                hv = self.HexaVertices[i]
                vtx_idx = [idx_grid[j][null[j] + self.sc_shift[hv[j]]] for j in range(3)]
                vtx_xyz = [xyz[j][vtx_idx[j]] for j in range(3)]
                d['B' + stri] = B[tuple(vtx_idx)]
                vtx_coords.append(np.array(vtx_xyz))
                B_fields.append(np.array(B[tuple(vtx_idx)]))

            if self.verbose:
                print('null', null)
                print('vtk_coords', vtx_coords)
                print('B', B_fields)

            # Find out the null type and gradB eigenvalues
            e_vals, _ = np.linalg.eig(field_gradient_hexa(vtx_coords, B_fields))
            d['xyz'] = null_location(np.array(vtx_coords), np.array(B_fields))
            d['lambda'] = e_vals
            d['T'] = null_point_type(e_vals, d['xyz'])
            res.append(d)

        # Save nulls to nulls_found, sort them and return a _copy_ of nulls_found
        self.nulls_found = list(res)

        return res

    def findNullsVTK(self, it=None, split=[2, 2, 2]):
        """ Locate nulls in a VTK file that contains vector (B) field.

        """
        time.clock()
        if it is not None:
            self.input_file = os.path.join(self.input_path, self.input_prefix + vtk_utils.fname_suffix(it, fill_zeros=0) + '.vtk')

        print('Reading file ' + self.input_file)

        B, xyz = vtk_utils.readVTK(self.input_file, ['B'])
        B = B['B']

        #
        # TODO: Split the whole array into parts and locate nulls in each of them
        #
        nulls_found = self.findNullsHexa(B, xyz=xyz)

        print('Nulls found:', len(nulls_found))
        print('Time elapsed:', time.clock())

        self.nulls_found = nulls_found
        self.dxyz = [xyz[0][1, 0, 0] - xyz[0][0, 0, 0], xyz[1][0, 1, 0] - xyz[1][0, 0, 0], xyz[2][0, 0, 1] - xyz[2][0, 0, 0]]

        return nulls_found

    def findNullsPVTR(self, it=None):
        """ Locate nulls in a PVTR file created with proc2vtr.py or vtk2vtr.py.
        Good thing about PVTR files is they can be processed slice-by-slice, even on a laptop.

        TODO: I haven't tested it since implementing preselection of cells and real null locations
        instead of indices in the grid.

        """
        time.clock()
        if it is not None:
            self.input_file = os.path.join(self.input_path, self.input_prefix + vtk_utils.fname_suffix(it, fill_zeros=8) + '.pvtr')

        print('Reading file ' + self.input_file)

        reader = vtk_utils.iPicPVTRReader()
        reader.parse(self.input_file)
        nulls_found = []

        for VTRIndex in range(len(reader.parts)):
            print('Part ' + str(VTRIndex))

            # Read the B field
            B, VTRCoords = reader.ReadPart(VTRIndex, Arrays='B', Coords=True)

            B = B['B']
            B = B.swapaxes(0,2) # [x,y,z,3]

            # Get the list of nulls
            nulls = self.findNullsHexa(B, xyz=VTRCoords)

            nulls_found.extend(nulls)

            print('Nulls found: ', len(nulls))
        print('Time elapsed: ', time.clock())

        self.nulls_found = nulls_found
        self.dxyz = [x[1] - x[0] for x in VTRCoords]

        return nulls_found

    def get_null_count(self, nulls=None):
        """ Compute the number of spiral and radial nulls. """
        if not nulls:
            nulls = self.nulls_found

        null_count = {t: 0 for t in self.null_types}
        for n in nulls:
            null_count[n['T']] += 1

        return null_count

    def sort_nulls(self):
        """ Sort the list of nulls by topological type """
        #self.nulls_found = sorted(self.nulls_found, key=lambda null: null['T'] + repr(null['lambda']))
        self.nulls_found = sorted(self.nulls_found, key=lambda null: null['xyz'][-1])

    def get_null_sequence(self, indices, path=None, prefix=None, suffix_gen=None):
        """ Only returns the null times for each timestep, without plotting """
        if not path:
            path = self.output_path

        if not prefix:
            prefix = self.output_prefix

        if not suffix_gen:
            suffix_gen = vtk_utils.fname_suffix

        if not indices:
            all_files = glob(os.path.join(path, 'nulls*.csv'))
            indices = sorted([int(((os.path.split(f)[1]).split('.')[0])[5:]) for f in all_files])
        else:
            all_files = [os.path.join(path, prefix + suffix_gen(i) + '.csv') for i in indices]

        total_count = {t: [] for t in self.null_types}
        total_count['As+Bs/total'] = []
        for i in range(len(indices)):
            self.readCSV(all_files[i])
            null_count = self.get_null_count()

            for t in self.null_types:
                total_count[t].append(null_count[t])

            total_count['As+Bs/total'].append(float(null_count['As'] + null_count['Bs']) / (null_count['A'] + null_count['B'] + null_count['As'] + null_count['Bs']))

        return indices, total_count

    def plot_null_types(self, indices, path=None, prefix=None, suffix_gen=None):
        """ Plot time-series of nulls """
        indices, total_count = self.get_null_sequence(indices, path, prefix, suffix_gen)
        fig = plt.figure('Null types')
        ax = fig.add_subplot(111)

        # First scatter the spiral and radial null numbers
        for t in ['A', 'B', 'As', 'Bs']: #null_types:
            if any(total_count[t]):
                plt.plot(indices, total_count[t], label=t, marker=self.null_symbols[t], fillstyle='full', color=self.null_colors[t], markeredgecolor=self.null_colors[t], markeredgewidth=0.0, markersize=10, linewidth=4)

        ax.set_ylim(0, plt.ylim()[1])
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        ax.set_ylabel(r'# of nulls')
        ax.set_xlabel(r'it')


        # On the right axis, plot the fraction of spiral nulls
        ax2 = ax.twinx()
        ax2.set_ylim(0, 101)
        ax2.set_ylabel('% spiral')
        ax2.plot(indices, 100. * np.array(total_count['As+Bs/total']), label='% spiral', linewidth=4, color='black')

        # Show legends of all lines in both axes
        lns = ax.lines + ax2.lines
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc=0, ncol=1, frameon=False, borderaxespad=0.01, handletextpad=0.01)

        plt.tight_layout()

        return indices

    def readCSV(self, csv_name):
        """ Read the CSV with nulls.

        In December 2018 I removed saving/reading of the coordinates of the vertices enclosing the null.
        Only the x, y, z of the null are written.

        """
        nulls = []
        with open(csv_name, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                n = {}
                n['lambda'] = np.array([complex(row.get('lambda'+repr(j), '0')) for j in range(1, 4)])
                n['T'] = row.get('type', 'O').strip()
                if ('x' in row) and ('y' in row) and ('z' in row):
                    n['xyz'] = np.array([float(row[k]) for k in ['x', 'y', 'z']])

                # Append to the nulls list
                nulls.append(n)

        self.nulls_found = nulls
        return nulls

    def writeCSV(self, fname=r'', it=None, dxyz=[], xyz0=[], format_output=False,
                 extra_keys = []):
        """ Save the nulls_found to a comma-separated value file.
            If file name isn't specified, it is taken from the timestep.

        Parameters
            xyz0 - the coordinates of the lower left vertex of the grid
            format_output - when true, the floats will be formatted. You'll loose precision, but get a nicer looking file.

        """
        print('Saving nulls to csv...')

        if not self.nulls_found:
            print('writeCSV: nothing to save!')
            return None

        if not fname:
            if it is not None:
                fname = os.path.join(self.output_path, self.output_prefix + vtk_utils.fname_suffix(it, fill_zeros=8) + '.csv')
            else:
                raise ValueError('writeCSV: No output file name or time step specified!')

        # if grid dimensions not specified, take self.dxyz. If that is unspecified too, use [1, 1, 1].
        dxyz = dxyz if dxyz else self.dxyz
        dxyz = np.array(dxyz if dxyz else [1., 1., 1.])

        xyz0 = np.array(xyz0 if xyz0 else [0., 0., 0.])

        # Create destination directories if they don't exist
        if os.path.dirname(fname):
            os.makedirs(os.path.dirname(fname), exist_ok=True)

        with open(fname, 'w') as csv_file:
            writer = csv.writer(csv_file)
            row = ['type', 'x', 'y', 'z', 'lambda1', 'lambda2', 'lambda3'] + extra_keys
            writer.writerow(row)

            for n in self.nulls_found:
                # Null type
                row = [n['T'], ]
                xyz = xyz0 + dxyz*n['xyz']
                if format_output:
                    # Null coordinates
                    row.extend(['%5.2f' % x for x in xyz])
                    # Eigenvalues
                    #row.extend([('%2.2e' % a.real) + (' + ' if (a.imag > 0) else ' - ') + ('%2.2e' % np.abs(a.imag)) + 'j' for a in n['lambda']])
                    row.extend([('%.2f' % a.real) + ('+' if (a.imag > 0) else '-') + ('%.2f' % np.abs(a.imag)) + 'j' for a in n['lambda']])
                else:
                    row.extend(xyz)
                    row.extend(n['lambda'])

                row.extend([n[k] for k in extra_keys])

                writer.writerow(row)

        print('Written file: ' + fname)

    def writeVTU(self, fname=None, it=None, dxyz=None, xyz0 = None):
        """ Save nulls_found as an unstructured VTK.

            How to display the nulls in ParaView.

            1. Open ParaView
            2. Open the corresponding vtr, vtk, etc., file with fields (needed to correctly set the scales only)
            3. Open the nullsXXXXXXXX.vtu (or *.pvd)
            4. Apply filter "Glyph" to the nulls (ParaView 4.3, earlier versions may differ):
                - Select "spheres"
                - Select "show all points"
                - Set the desired scaling value
                - Click "Apply"
                - Select palette "Cold and Hot"
                - Choose discrete colors
                - Rescale to the values 1-6 (to have all range from X to O nulls, see below)
                - Enter the "Annotations" for the color values: {'X': 1, 'A': 2, 'B': 3, 'Bs': 4, 'As': 5, 'O': 6}
            The colors define the topological types of the null points.

        """
        if not self.nulls_found:
            print('writeVTU: nothing to save!')
            return None

        print('Saving nulls to vtu...')

        if not fname:
            if it is not None:
                fname = os.path.join(self.output_path, self.output_prefix + vtk_utils.fname_suffix(it, fill_zeros=8) + '.vtu')
            else:
                raise ValueError('writeVTU: No output file name or time step specified!')

        if not os.path.exists(os.path.split(fname)[0]):
            os.makedirs(os.path.split(fname)[0])

        if dxyz is None:
            dxyz = self.dxyz
        dxyz = np.array(dxyz if dxyz else [1., 1., 1.])

        if xyz0 is None:
            xyz0 = [0., 0., 0]
        xyz0 = np.array(xyz0)

        # Now save the VTU file
        vector_data = {'lambda': [n['lambda'] for n in self.nulls_found]}
        scalar_data = {'color': [self.null_colors_num[n['T']] for n in self.nulls_found]}
        vtk_utils.writeVTU(fname, scalar_data, vector_data, [dxyz * n['xyz'] + xyz0 for n in self.nulls_found])

        print('Written VTU file: ' + fname)